import React from 'react';
import { View, Text, StyleSheet, TextInput,ScrollView,  TouchableOpacity } from 'react-native';
import {LinearGradient} from 'react-native-linear-gradient';
class qq extends React.Component {
  constructor(){
    super();
    this.state = {dwidth:300};
  }
  componentWillMount(){
  }
  render() {
    return (
      <View style={{ flex: 1}}>
   
<LinearGradient
          style={{ flex: 1 , justifyContent:'center', alignItems:'center'}}
          colors={['#b866d1', '#3524ae']}
          start={[0.8, 0.9]}
          end={[0.1, 0.2]}
        >
          <View style={style.hexagon}>
            <View style={style.hexagonInner} >
              <FontAwesome 
                name="microphone"
                size={50}

              />
            </View>
            <View style={style.hexagonBefore} />
            <View style={style.hexagonAfter} />
          </View>
        </LinearGradient>
          <View style={[style.boxStyle,{width:this.state.dwidth}]} >
            <Text style={{textAlign:'center'}}>Login Page</Text>
            <View style={{marginTop:30}}>
            <TextInput
              placeholder="Email"
              style={{borderBottomColor:'#ccc', borderBottomWidth:1,paddingLeft:25}}
            />
            <FontAwesome 
              name="user"
              size={20}
              style={{position:'absolute',bottom:6}}
            />
            </View>
            <View style={{marginTop:30}}>
            <TextInput
              placeholder="Password"
              style={{borderBottomColor:'#ccc', borderBottomWidth:1,paddingLeft:25}}
              secureTextEntry
            />
            <FontAwesome 
              name="lock"
              size={20}
              style={{position:'absolute',bottom:6}}
            />
            </View>
            <View style={{marginTop:'auto', marginBottom:-25, bottom:-25}}>
            <TouchableOpacity>
            <LinearGradient
              style={{ width:200, height:50, alignSelf:'center', borderRadius:10 }}
              colors={['#b866d1', '#3524ae']}
              start={[0.8, 0.9]}
              end={[0.1, 0.2]}
            />
            </TouchableOpacity>
            </View>
          </View>

      </View>
    );
  }
}
const style = StyleSheet.create({
  boxStyle: {
    padding:25,
    height: 300,
    alignSelf: 'center',
    marginTop: -50,
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 20,
  },
  hexagon: {
    width: 100,
    height: 55
  },
  hexagonInner: {
    width: 100,
    height: 55,
    backgroundColor: 'white',
    justifyContent:'center',
    alignItems:'center'
  },
  hexagonAfter: {
    position: 'absolute',
    bottom: -25,
    left: 0,
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderLeftWidth: 50,
    borderLeftColor: 'transparent',
    borderRightWidth: 50,
    borderRightColor: 'transparent',
    borderTopWidth: 25,
    borderTopColor: 'white'
  },
  hexagonBefore: {
    position: 'absolute',
    top: -25,
    left: 0,
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderLeftWidth: 50,
    borderLeftColor: 'transparent',
    borderRightWidth: 50,
    borderRightColor: 'transparent',
    borderBottomWidth: 25,
    borderBottomColor: 'white'

  }
});
export default qq;