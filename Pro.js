import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content, Header,Card, CardItem, Body } from 'native-base';
import User from './User';
class Pro extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <Container>
          <Header></Header>
          <Content padder>
            <User name="James Bond" details="This is my Text" designation="Web developer" color="red" image="https://artofidealism.files.wordpress.com/2012/01/person-pawn-nitichan-_ubuntu.jpg"/>
            <User name="Harry Potter" details="Harry Potter Text" designation="Mobile developer" color="green" />
            <User name="Harry Potter" details="Harry Potter Text" designation="Mobile developer" color="yellow" />
            <User name="Harry Potter" details="Harry Potter Text" designation="Mobile developer" color="gray" />
          </Content>
    </Container>
    );
  }
}

export default Pro;
