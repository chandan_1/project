import React, { Component } from 'react';
import { View, Text,Image } from 'react-native';
import firebase from 'react-native-firebase';
import {Container, Content, Spinner ,Header, Item, Form, Input, Label, Button} from 'native-base';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:'iam@gmail.com',
        password:'123456',
        temp:0
        
    };
  }
  getTemp(){
    fetch('http://api.openweathermap.org/data/2.5/weather?lat=30.7046&lon=76.7179&appid=47fbd81d25c97dda698f2b71d18e57bf&units=metric')
    .then((data) => data.json())
    .then((res) => {
      this.setState({temp:res.main.temp});
    })
  }
  login()
  {
    firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
    .then((data)  => {
      this.props.navigation.navigate('Home');
    })
    .catch((err) => {
      alert(err);
    })
  }
  componentDidMount(){
    this.getTemp();
  }
  render() {
    return (
      <Container>
      <Content padder>
      <Image
  source={ {uri:'http://www.sachtechsolution.com/images/logo.png'} }
  style={{ width: 200, height: 200 }}
/>
          <Text>Login</Text>
          <Form>
              <Item floatingLabel>
                  <Label>Email</Label>
                  <Input 
                    onChangeText={(text) => this.setState({email:text})}
                    value={this.state.email}
                  />
              </Item>
              <Item floatingLabel>
                  <Label>Password</Label>
                  <Input 
                    onChangeText={(text) => this.setState({password:text})}
                    value={this.state.password}
                    secureTextEntry
                  />
              </Item>
              <Button
              success block
              onPress={() => this.login()}
              >
                  <Text>Login</Text>
              </Button>
              <Text>{this.state.temp}</Text>
          </Form>
      </Content>
  </Container>
    );
  }
}

export default Login;