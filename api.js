import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content, Header } from 'native-base';
class api extends Component {
  constructor(props) {
    super(props);
    this.state = {  
        data:[]
    };
  }
  fetchData(){
    fetch('http://api.nightlights.io/months/1993.3-1993.4/states')
    .then((data) => data.json())
    .then((res) => {
        this.setState({data:res});
    })
  }
  componentDidMount(){
      this.fetchData();
  }
  render() {
    return (
        <Container>
            <Header></Header>
            <Content>
                {this.state.data.map((item) => {
                    return(
                        <Text style={{padding:15, borderColor:'red', borderWidth:2}}>{item.key}</Text>
                    )
                })}
            </Content>
        </Container>
    );
  }
}

export default api;