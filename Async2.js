import React, { Component } from 'react';
import { View, Text, TextInput, Button } from 'react-native';
import AsyncStorage  from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";

class Async2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginuser:'',
      loginpass:'',
      ruser:'',
      rpass:'',
      alldata:[],
      color:'red'
    };
    this.arr = []
  }
  
    register(){
    let user = {username:this.state.ruser, password:this.state.rpass};
    this.arr.push(user);
    this.setState({alldata: this.arr}, () => {
        let str = JSON.stringify(this.state.alldata);
        AsyncStorage.setItem('users',str);
        alert('Success');
    });
    
}
login(){
  let login = false;
  for(i in this.arr){
      if(this.arr[i].username == this.state.loginuser){
          if(this.arr[i].password==this.state.loginpass){
             login = true; 
          }
      }
  }
  
  if(login){
    alert('Success');
        }
else{
    alert('User Not Found');
    }
}
  

  componentDidMount = async () => {
    let data = await AsyncStorage.getItem('users');
    let d = JSON.parse(data);
    this.arr = d;
this.setState({alldata:d});
}

  render() {
    return (
      <View style={{flex:1}}>
      <View style={{flex:1}}>
          <TextInput 
              onChangeText={(t) => this.setState({loginuser:t})}
              placeholder="Username"
          />
          <TextInput 
              onChangeText={(t) => this.setState({loginpass:t})}
              placeholder="Password"
          />
          <Button 
              title="Login"
              onPress={() => this.login()}
          />
          <Text>{JSON.stringify(this.state.alldata)}</Text>
      </View>
      <View style={{flex:1}}>
          <TextInput 
              onChangeText={(t) => this.setState({ruser:t})}
              placeholder="Username"
          />
          <TextInput 
              onChangeText={(t) => this.setState({rpass:t})}
              placeholder="Password"
          />
          <Button 
              title="Register"
              onPress={() => this.register()}
          />
      </View>
    </View>
    );
  }
}

export default Async2;