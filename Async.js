import React, { Component } from 'react';
import { View, Text, TextInput, Button } from 'react-native';
import AsyncStorage  from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
class Async extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loginuser:'',
        loginpass:'',
        ruser:'',
        rpass:'',
        alldata:[],
        color:'red'
    };
    this.arr = [];
    this._interval = null;
  }
  login(){
    let login = false;
    for(i in this.arr){
        if(this.arr[i].username == this.state.loginuser){
            if(this.arr[i].password==this.state.loginpass){
               login = true; 
            }
        }
    }
    
    if(login){
        alert('Success');
            }
    else{
        alert('User Not Found');
        }
  }
  register(){
      let user1 = {username:this.state.ruser, password:this.state.rpass};
      this.arr.push(user1);
      this.setState({alldata: this.arr}, () => {
          let str = JSON.stringify(this.state.alldata);
          AsyncStorage.setItem('users',str);
          alert('Success');
      });
      
  }
  colorfun(){
    let connected = navigator.onLine;
    console.log(connected);
    if(connected){
        this.setState({color:'green'});
        console.log('connected');
    }else{
        this.setState({color:'red'});
        console.log('not');
    }
  }
  componentDidMount = async () => {
      let data = await AsyncStorage.getItem('users');
      let d = JSON.parse(data);
      if(d!=null){
        this.arr = d;
      }
      this._interval = setInterval(() => {
            NetInfo.fetch().then(state => {
            if(state.isConnected){
                this.setState({color:'green'});
            }else{
                    this.setState({color:'red'});
            }
            });
        }, 1000);  
      this.setState({alldata:d});
  }
  render() {
    return (
      <View style={{flex:1}}>
        <View style={{flex:1, backgroundColor:this.state.color}}>
            <TextInput 
                onChangeText={(t) => this.setState({loginuser:t})}
                placeholder="Username"
            />
            <TextInput 
                onChangeText={(t) => this.setState({loginpass:t})}
                placeholder="Password"
            />
            <Button 
                title="Login"
                onPress={() => this.login()}
            />
            <Text>{JSON.stringify(this.state.alldata)}</Text>
        </View>
        <View style={{flex:1}}>
            <TextInput 
                onChangeText={(t) => this.setState({ruser:t})}
                placeholder="Username"
            />
            <TextInput 
                onChangeText={(t) => this.setState({rpass:t})}
                placeholder="Password"
            />
            <Button 
                title="Register"
                onPress={() => this.register()}
            />
        </View>
      </View>
    );
  }
}

export default Async;