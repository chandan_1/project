import React, { Component } from 'react';
import { View, Text,Image} from 'react-native';
import firebase from 'react-native-firebase';
import { Container,Header,Button,Left,Body,Right } from 'native-base';
import { Input } from 'react-native-elements';

class input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CollegeName:'',
      selectstate:''
    };
  }

  
  addcollege()
  {
    firebase.firestore().collection('college').add(this.state)
    .then(() =>{
      alert('college Added');
    })
    .catch((err) =>{
      alert(err);
    })
  }
  render() {
    return (
      <View
      style={{}}
      >
        <Header>
          <Left>
          <Image
          source={require('./menu1.png')}
          style={{width:50,height:50}}
          /> 
          </Left>
          <Body>
          <Text
          style={{fontSize:25}}
          >
             add College
          </Text>
          </Body>
        </Header>

       <Text
       style={{paddingTop:15,fontSize:20,paddingLeft:5  }}
       >
         College Name
         </Text> 
        <Input
        onChangeText={ (t) => this.setState({CollegeName:t})}
        ></Input>
        <Text
       style={{paddingTop:15,fontSize:20,paddingLeft:5  }}
       >
         Select State
       </Text>
       <Input
               onChangeText={ (t) => this.setState({selectstate:t})}
       ></Input>
       <Button
       block
       danger
       rounded    
       onPress={() => this.addprofile()}
       style={{width:200,alignSelf:'center',marginTop:50}
      }
       ></Button>
        </View>
    );
  }
}
export default input;