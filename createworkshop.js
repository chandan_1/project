import React, { Component } from 'react';
import { View, Text ,TextInput} from 'react-native';
import { Input ,Button} from 'native-base';
import firebase from 'react-native-firebase';

class createworkshop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',
      class:'',
      address:''
    };
  }

data()
{
  firebase.firestore().collection('users').add({ 
    name:this.state.name,
    class:this.state.class,
    address:this.state.address
    
  })
  .then(() => {
    alert('added');
  })
  .catch((err) => {
    alert(err);
  })
}
componentDidMount(){
  this.data();
}
  render() {
    return (
      <View>
        <TextInput
        onChangeText={(t) => this.setState({name:t})}
        style={{borderWidth:0.5}}
        />
        <TextInput
        onChangeText={(t) => this.setState({class:t})}
        style={{borderWidth:0.5}}
        />  
        <TextInput
        onChangeText={(t) => this.setState({address:t})}
        style={{borderWidth:0.5}}
        />
        <Button
        onPress={() => {
          this.data();
        }}
        >
          <Text>
            data store
          </Text>
        </Button>
      </View>
    );
  }
}

export default createworkshop;